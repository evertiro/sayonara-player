/* LastFM.cpp */

/* Copyright (C) 2011-2019 Lucio Carreras
 *
 * This file is part of sayonara player
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Gfeneral Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


/*
 * LastFM.cpp
 *
 *  Created on: Apr 19, 2011
 *      Author: Lucio Carreras
 */

#include "LastFM.h"
#include "LFMGlobals.h"
#include "LFMTrackChangedThread.h"
#include "LFMLoginThread.h"
#include "LFMWebAccess.h"

#include "Utils/Algorithm.h"
#include "Utils/RandomGenerator.h"
#include "Utils/Playlist/PlaylistMode.h"
#include "Utils/Settings/Settings.h"
#include "Utils/MetaData/MetaDataList.h"
#include "Utils/Logger/Logger.h"
#include "Utils/Crypt.h"

#include "Components/PlayManager/PlayManager.h"
#include "Components/Playlist/Playlist.h"
#include "Components/Playlist/PlaylistHandler.h"

#include "Database/Connector.h"
#include "Database/LibraryDatabase.h"

#include <QDomDocument>
#include <QUrl>
#include <QTimer>

#include <ctime>

namespace Algorithm=Util::Algorithm;

using namespace LastFM;

struct Base::Private
{
	QString						session_key;

	QTimer*						scrobble_timer=nullptr;
	QTimer*						track_changed_timer=nullptr;

	TrackChangedThread*			track_changed_thread=nullptr;

	bool						logged_in;

	Private(QObject* parent) :
		track_changed_thread(new TrackChangedThread(parent)),
		logged_in(false)
	{
		scrobble_timer = new QTimer();
		scrobble_timer->setSingleShot(true);

		track_changed_timer = new QTimer();
		track_changed_timer->setSingleShot(true);
	}
};

Base::Base() :
	QObject()
{
	m = Pimpl::make<Private>(this);

	connect(m->scrobble_timer, &QTimer::timeout, this, &Base::scrobble);
	connect(m->track_changed_timer, &QTimer::timeout, this, &Base::track_changed_timer_timed_out);

	connect(PlayManager::instance(), &PlayManager::sig_track_changed,	this, &Base::current_track_changed);
	connect(m->track_changed_thread, &TrackChangedThread::sig_similar_artists_available,
			this, &Base::similar_artists_fetched);

	ListenSetting(Set::LFM_Active, Base::lfm_active_changed);
}

Base::~Base() = default;

bool Base::is_logged_in()
{
	return m->logged_in;
}

void Base::login(const QString& username, const QString& password)
{
	auto* login_thread = new LoginThread(this);

	connect(login_thread, &LoginThread::sig_logged_in, this, &Base::login_thread_finished);
	connect(login_thread, &LoginThread::sig_error, this, [=](const QString& error_message){
		sp_log(Log::Warning, this) << error_message;
		emit sig_logged_in(false);
	});

	login_thread->login(username, password);
}


void Base::lfm_active_changed()
{
	m->logged_in = false;

	bool active = GetSetting(Set::LFM_Active);
	if(active)
	{
		QString username = GetSetting(Set::LFM_Username);
		QString password = Util::Crypt::decrypt(GetSetting(Set::LFM_Password));

		login(username, password);
	}
}

void Base::login_thread_finished(bool success)
{
	m->logged_in = success;

	if(!success){
		emit sig_logged_in(false);
		return;
	}

	auto* login_thread = static_cast<LoginThread*>(sender());
	LoginStuff login_info = login_thread->getLoginStuff();

	m->logged_in = login_info.logged_in;
	m->session_key = login_info.session_key;

	SetSetting(Set::LFM_SessionKey, m->session_key);

	sp_log(Log::Debug, this) << "Got session key";

	if(!m->logged_in){
		sp_log(Log::Warning, this) << "Cannot login";
	}

	emit sig_logged_in(m->logged_in);

	sender()->deleteLater();
}

void Base::current_track_changed(const MetaData& md)
{
	Q_UNUSED(md)

	Playlist::Mode pl_mode = GetSetting(Set::PL_Mode);
	if( Playlist::Mode::isActiveAndEnabled(pl_mode.dynamic()))
	{
		m->track_changed_timer->stop();
		m->track_changed_timer->start(1000);
	}

	// scrobble
	if(GetSetting(Set::LFM_Active) && m->logged_in)
	{
		int secs = GetSetting(Set::LFM_ScrobbleTimeSec);
		if(secs > 0)
		{
			m->scrobble_timer->stop();
			m->scrobble_timer->start(secs * 1000);
		}
	}
}


void Base::scrobble()
{
	if(!GetSetting(Set::LFM_Active) || !m->logged_in) {
		return;
	}

	MetaData md = PlayManager::instance()->current_track();
	if(md.title().isEmpty() || md.artist().isEmpty()){
		return;
	}

	sp_log(Log::Debug, this) << "Scrobble " << md.title() << " by " << md.artist();

	auto* lfm_wa = new WebAccess();
	connect(lfm_wa, &WebAccess::sig_response, this, &Base::scrobble_response_received);
	connect(lfm_wa, &WebAccess::sig_error, this, &Base::scrobble_error_received);

	time_t rawtime = time(nullptr);
	struct tm* ptm = localtime(&rawtime);
	time_t started = mktime(ptm);

	UrlParams sig_data;
	sig_data["api_key"] =		LFM_API_KEY;
	sig_data["artist"] =		md.artist().toLocal8Bit();
	sig_data["duration"] =		QString::number(md.duration_ms() / 1000).toLocal8Bit();
	sig_data["method"] =		"track.scrobble";
	sig_data["sk"] =			m->session_key.toLocal8Bit();
	sig_data["timestamp"] =		QString::number(started).toLocal8Bit();
	sig_data["track"] =			md.title().toLocal8Bit();

	sig_data.append_signature();

	QByteArray post_data;
	QString url = lfm_wa->create_std_url_post("http://ws.audioscrobbler.com/2.0/", sig_data, post_data);

	lfm_wa->call_post_url(url, post_data);
}

void Base::scrobble_response_received(const QByteArray& data) {	Q_UNUSED(data) }

void Base::scrobble_error_received(const QString& error)
{
	sp_log(Log::Warning, this) << "Scrobble: " << error;
}

void Base::track_changed_timer_timed_out()
{
	MetaData md = PlayManager::instance()->current_track();

	if(md.radio_mode() == RadioMode::Off)
	{
		m->track_changed_thread->search_similar_artists(md);
	}

	if(GetSetting(Set::LFM_Active) && m->logged_in)
	{
		m->track_changed_thread->update_now_playing(m->session_key, md);
	}
}

// private slot
void Base::similar_artists_fetched(IdList artist_ids)
{
	if(artist_ids.isEmpty()){
		return;
	}

	auto* db = DB::Connector::instance();
	DB::LibraryDatabase* lib_db = db->library_db(-1, 0);

	auto* plh = Playlist::Handler::instance();

	int active_idx = plh->active_index();
	PlaylistConstPtr active_playlist = plh->playlist(active_idx);

	if(!active_playlist){
		return;
	}

	const MetaDataList& v_md = active_playlist->tracks();

	std::random_shuffle(artist_ids.begin(), artist_ids.end());

	for( auto it=artist_ids.begin(); it != artist_ids.end(); it++ )
	{
		MetaDataList artist_tracks;
		lib_db->getAllTracksByArtist(IdList{*it}, artist_tracks);

		std::random_shuffle(artist_tracks.begin(), artist_tracks.end());

		// try all songs of artist
		for(int rounds=0; rounds < artist_tracks.count(); rounds++)
		{
			int rnd_track = RandomGenerator::get_random_number(0, int(artist_tracks.size())- 1);

			MetaData md = artist_tracks.take_at(rnd_track);

			// two times the same track is not allowed
			bool track_exists = Algorithm::contains(v_md, [md](const MetaData& it_md){
				return (md.id() == it_md.id());
			});

			if(!track_exists){
				MetaDataList v_md; v_md << md;

				plh->append_tracks(v_md, active_idx);
				return;
			}
		}
	}
}

