/* PlayManager.cpp */

/* Copyright (C) 2011-2019  Lucio Carreras
 *
 * This file is part of sayonara player
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "PlayManager.h"
#include "Components/Tagging/ChangeNotifier.h"

#include "Interfaces/Notification/NotificationHandler.h"

#include "Utils/Algorithm.h"
#include "Utils/MetaData/MetaData.h"
#include "Utils/MetaData/MetaDataList.h"
#include "Utils/Settings/Settings.h"
#include "Utils/Logger/Logger.h"
#include "Utils/FileUtils.h"

#include <QDateTime>
#include <QTime>

#include <array>

const int InvalidTimeStamp=-1;

template<typename T, int N_ITEMS>
class RingBuffer
{
private:
	size_t _cur_idx;
	size_t _n_items;
	std::array<T, N_ITEMS> _data;

public:
	RingBuffer()
	{
		clear();
	}

	void clear()
	{
		_cur_idx = 0;
		_n_items = 0;
	}

	void insert(const T& item)
	{
		_data[_cur_idx] = item;
		_cur_idx = (_cur_idx + 1) % N_ITEMS;
		_n_items = std::min<size_t>(N_ITEMS, _n_items + 1);
	}

	bool has_item(const T& item) const
	{
		auto it = std::find(_data.begin(), _data.end(), item);
		return (it != _data.end());
	}

	int count() const
	{
		return int(_n_items);
	}

	bool is_empty() const
	{
		return (count() == 0);
	}
};


struct PlayManager::Private
{
	MetaData				md;
	RingBuffer<QString, 3>	ring_buffer;
	int						track_idx;
	MilliSeconds			position_ms;
	MilliSeconds			initial_position_ms;
	MilliSeconds			track_playtime_ms;
	PlayState				playstate;

	Private()
	{
		reset();
		playstate = PlayState::FirstStartup;
	}

	void reset()
	{
		md = MetaData();
		ring_buffer.clear();
		track_idx = -1;
		position_ms = 0;
		track_playtime_ms = 0;
		initial_position_ms = InvalidTimeStamp;
		playstate = PlayState::Stopped;
	}
};


PlayManager::PlayManager(QObject* parent) :
	QObject(parent)
{
	m = Pimpl::make<Private>();

	bool load_playlist = (GetSetting(Set::PL_LoadSavedPlaylists) || GetSetting(Set::PL_LoadTemporaryPlaylists));
	bool load_last_track = GetSetting(Set::PL_LoadLastTrack);
	bool remember_last_time = GetSetting(Set::PL_RememberTime);

	if(load_playlist && load_last_track)
	{
		if(remember_last_time) {
			m->initial_position_ms = GetSetting(Set::Engine_CurTrackPos_s) * 1000;
		}

		else {
			m->initial_position_ms = 0;
		}
	}

	else {
		m->initial_position_ms = InvalidTimeStamp;
	}

	auto* mdcn = Tagging::ChangeNotifier::instance();
	connect(mdcn, &Tagging::ChangeNotifier::sig_metadata_changed, this, &PlayManager::track_metadata_changed);
	connect(mdcn, &Tagging::ChangeNotifier::sig_metadata_deleted, this, &PlayManager::tracks_deleted);
}

PlayManager::~PlayManager() = default;

PlayState PlayManager::playstate() const
{
	return m->playstate;
}

MilliSeconds PlayManager::current_position_ms() const
{
	return m->position_ms;
}

MilliSeconds PlayManager::current_track_playtime_ms() const
{
	return m->track_playtime_ms;
}

MilliSeconds PlayManager::initial_position_ms() const
{
	return m->initial_position_ms;
}

MilliSeconds PlayManager::duration_ms() const
{
	return m->md.duration_ms();
}

Bitrate PlayManager::bitrate() const
{
	return m->md.bitrate();
}

const MetaData& PlayManager::current_track() const
{
	return m->md;
}

int PlayManager::volume() const
{
	return GetSetting(Set::Engine_Vol);
}

bool PlayManager::is_muted() const
{
	return GetSetting(Set::Engine_Mute);
}


void PlayManager::play()
{
	m->playstate = PlayState::Playing;
	emit sig_playstate_changed(m->playstate);
}

void PlayManager::wake_up()
{
	emit sig_wake_up();
}

void PlayManager::play_pause()
{
	if(m->playstate == PlayState::Playing) {
		pause();
	}

	else if(m->playstate == PlayState::Stopped) {
		wake_up();
	}

	else {
		play();
	}
}


void PlayManager::pause()
{
	m->playstate = PlayState::Paused;
	emit sig_playstate_changed(m->playstate);
}


void PlayManager::previous()
{
	emit sig_previous();
}


void PlayManager::next()
{
	emit sig_next();
}


void PlayManager::stop()
{
	m->reset();

	emit sig_playstate_changed(m->playstate);
}


void PlayManager::record(bool b)
{
	if(GetSetting(SetNoDB::MP3enc_found)) {
		emit sig_record(b);
	} else {
		emit sig_record(false);
	}
}

void PlayManager::seek_rel(double percent)
{
	emit sig_seeked_rel(percent);
}

void PlayManager::seek_rel_ms(MilliSeconds ms)
{
	emit sig_seeked_rel_ms(ms);
}

void PlayManager::seek_abs_ms(MilliSeconds ms)
{
	emit sig_seeked_abs_ms(ms);
}

void PlayManager::set_position_ms(MilliSeconds ms)
{
	MilliSeconds difference = (ms - m->position_ms);
	if(difference > 0 && difference < 1000) {
		m->track_playtime_ms += difference;
	}

	m->position_ms = ms;

	SetSetting(Set::Engine_CurTrackPos_s, int(m->position_ms / 1000));

	emit sig_position_changed_ms(ms);
}


void PlayManager::change_track(const MetaData& md, int track_idx)
{
	bool is_first_start = (m->playstate == PlayState::FirstStartup);

	m->md = md;
	m->position_ms = 0;
	m->track_playtime_ms = 0;
	m->track_idx = track_idx;
	m->ring_buffer.clear();

	// initial position is outdated now and never needed again
	if(m->initial_position_ms >= 0)
	{
		int old_idx = GetSetting(Set::PL_LastTrack);
		if(old_idx != m->track_idx) {
			m->initial_position_ms = InvalidTimeStamp;
		}
	}

	// play or stop
	if(m->track_idx >= 0)
	{
		emit sig_track_changed(m->md);
		emit sig_track_idx_changed(m->track_idx);

		if(!is_first_start)
		{
			play();

			if( (md.radio_mode() != RadioMode::Off) &&
					GetSetting(Set::Engine_SR_Active) &&
					GetSetting(Set::Engine_SR_AutoRecord) )
			{
				record(true);
			}
		}
	}

	else {
		sp_log(Log::Info, this) << "Playlist finished";
		emit sig_playlist_finished();
		stop();
	}

	if(!is_first_start)
	{
		// save last track
		if(md.db_id() == 0) {
			SetSetting(Set::PL_LastTrack, m->track_idx);
		}

		else {
			SetSetting(Set::PL_LastTrack, -1);
		}
	}

	// show notification
	if(GetSetting(Set::Notification_Show))
	{
		if(m->track_idx > -1 && !m->md.filepath().isEmpty())
		{
			NotificationHandler::instance()->notify(m->md);
		}
	}
}


void PlayManager::change_track_metadata(const MetaData& md)
{
	MetaData md_old = m->md;
	m->md = md;

	QString str = md.title() + md.artist() + md.album();
	bool has_data = m->ring_buffer.has_item(str);

	if(!has_data)
	{
		if(GetSetting(Set::Notification_Show)) {
			NotificationHandler::instance()->notify(m->md);
		}

		// only insert www tracks into the buffer
		if( m->ring_buffer.count() > 0 && Util::File::is_www(md.filepath()))
		{
			md_old.set_album("");
			md_old.set_disabled(true);
			md_old.set_filepath("");

			QDateTime date = QDateTime::currentDateTime();
			QTime time = date.time();
			md_old.set_duration_ms((time.hour() * 60 + time.minute()) * 1000);

			emit sig_www_track_finished(md_old);
		}
	}

	emit sig_track_metadata_changed();
}

void PlayManager::set_track_ready()
{
	if(m->initial_position_ms == InvalidTimeStamp) {
		return;
	}

	sp_log(Log::Debug, this) << "Track ready, Start at " << m->initial_position_ms / 1000 << "ms";
	if(m->initial_position_ms != 0)
	{
		this->seek_abs_ms(m->initial_position_ms);
	}

	m->initial_position_ms = InvalidTimeStamp;

	if(GetSetting(Set::PL_StartPlaying)){
		play();
	}

	else {
		pause();
	}
}

void PlayManager::set_track_finished()
{
	next();
}

void PlayManager::buffering(int progress)
{
	emit sig_buffer(progress);
}


void PlayManager::volume_up()
{
	set_volume(GetSetting(Set::Engine_Vol) + 5);
}

void PlayManager::volume_down()
{
	set_volume(GetSetting(Set::Engine_Vol) - 5);
}

void PlayManager::set_volume(int vol)
{
	vol = std::min(vol, 100);
	vol = std::max(vol, 0);
	SetSetting(Set::Engine_Vol, vol);
	emit sig_volume_changed(vol);
}

void PlayManager::set_muted(bool b)
{
	SetSetting(Set::Engine_Mute, b);
	emit sig_mute_changed(b);
}

void PlayManager::toggle_mute()
{
	bool muted = GetSetting(Set::Engine_Mute);
	set_muted(!muted);
}


void PlayManager::error(const QString& message)
{
	emit sig_error(message);
}

void PlayManager::change_duration(MilliSeconds ms)
{
	m->md.set_duration_ms(ms);
	emit sig_duration_changed();
}

void PlayManager::change_bitrate(Bitrate br)
{
	m->md.set_bitrate(br);
	emit sig_bitrate_changed();
}


void PlayManager::shutdown()
{
	if(m->playstate == PlayState::Stopped)
	{
		SetSetting(Set::PL_LastTrack, -1);
		SetSetting(Set::Engine_CurTrackPos_s, 0);
	}

	else {
		SetSetting(Set::Engine_CurTrackPos_s, int(m->position_ms / 1000));
	}
}


void PlayManager::track_metadata_changed()
{
	auto* mdcn = static_cast<Tagging::ChangeNotifier*>(sender());
	const QPair<MetaDataList, MetaDataList> changed_md = mdcn->changed_metadata();

	for(int i=0; i<changed_md.first.count(); i++)
	{
		const MetaData& md = changed_md.first[i];
		if(md == m->md){
			this->change_track_metadata(changed_md.second[i]);
			return;
		}
	}
}

void PlayManager::tracks_deleted()
{
	auto* mdcn = static_cast<Tagging::ChangeNotifier*>(sender());
	const MetaDataList v_md = mdcn->deleted_metadata();

	bool contains = Util::Algorithm::contains(v_md, [this](const MetaData& md){
		return (m->md.filepath() == md.filepath());
	});

	if(contains) {
		stop();
	}
}
