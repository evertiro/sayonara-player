#ifndef PLAYLISTTESTUTILS_H
#define PLAYLISTTESTUTILS_H

class MetaDataList;

namespace Test
{
	namespace Playlist
	{
		MetaDataList create_v_md(int min, int max);
	}
}

#endif // PLAYLISTTESTUTILS_H
